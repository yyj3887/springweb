package com.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PLAYER {
    String PLAYER_ID;
    String TEAM_ID;
    String PLAYER_NAME;
    String POSITION;
    String HEIGHT;
    String WEIGHT;
    String BIRTH_DATE;
}
