package com.example.firstapp.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.example.firstapp.service.get.GetFirstApp;

import lombok.RequiredArgsConstructor;

@CrossOrigin
@RestController("/")
@RequiredArgsConstructor
public class FirstAppControllerImpl implements FirstAppController {	
	final private GetFirstApp getFirstApp = null;	
	
	@Override
	public String GetHelloWorld() { 
		return getFirstApp.Get();
	}

}
