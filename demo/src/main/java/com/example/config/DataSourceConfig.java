package com.example.config;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {        

    String resource = "mybatis-config.xml";		
    SqlSessionFactory sqlSessionFactory;
    
    @Bean
    public SqlSessionFactory createSqlSessionFactory() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream(resource);		
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);		
        return sqlSessionFactory;
    }              
}
